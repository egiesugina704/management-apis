import { Component, OnInit ,ViewChild } from '@angular/core';
import { RestApiService } from '../../api.service';
import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { ListApis } from '../../field/list-apis';
import { Subject } from 'rxjs';
@Component({
	selector: 'app-apis-list',
	templateUrl: './apis-list.component.html',
	styleUrls: ['./apis-list.component.css']
})
export class ApisListComponent implements OnInit {
	
	dataSource = new MatTableDataSource<ListApis>();
	filterData;
	public displayedColumns = ['api_name', 'exeby','code','delete']
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	constructor(
		public restApi: RestApiService
		) { }
		// Get employees list
		loadEmployees() {
			return this.restApi.getAPI_LIST().subscribe((data: {}) => {
				this.dataSource.data = data as ListApis[];
			})
		}
		ngOnInit() {
			this.loadEmployees()
			this.dataSource = new MatTableDataSource<ListApis>();
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator
		}
		public doFilter = (value: string) => {
			this.dataSource.filter = value.trim().toLocaleLowerCase();
		}
		public redirectToDetails = (id: string) => {
			
		}
		
		public redirectToUpdate = (id: string) => {
			
		}
		
		public redirectToDelete = (id: string) => {
			
		}
		
	}