
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApisComponent } from './apis/apis.component';
import { ApisListComponent } from './apis-list/apis-list.component';
import { ApisFormComponent } from './apis-form/apis-form.component';
import { AppRoutingModule } from '../app-routing.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {MatButtonModule} from '@angular/material/button';
import { 
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
  MatFormFieldModule} from '@angular/material';
  import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
  import {FormsModule, ReactiveFormsModule} from '@angular/forms';
  import { BrowserModule } from '@angular/platform-browser';
  import { AngularMonacoEditorConfig, AngularMonacoEditorModule } from 'angular-monaco-editor';
  
  @NgModule({
    declarations: [ApisComponent, ApisListComponent, ApisFormComponent],
    imports: [
      CommonModule,
      AppRoutingModule,
      NgxPaginationModule,
      MatTableModule,
      MatSortModule,
      BrowserAnimationsModule,
      MatAutocompleteModule,
      MatButtonModule,
      MatButtonToggleModule,
      MatCardModule,
      MatCheckboxModule,
      MatChipsModule,
      MatDatepickerModule,
      MatDialogModule,
      MatExpansionModule,
      MatGridListModule,
      MatIconModule,
      MatInputModule,
      MatListModule,
      MatMenuModule,
      MatNativeDateModule,
      MatPaginatorModule,
      MatProgressBarModule,
      MatProgressSpinnerModule,
      MatRadioModule,
      MatRippleModule,
      MatSelectModule,
      MatSidenavModule,
      MatSliderModule,
      MatSlideToggleModule,
      MatSnackBarModule,
      MatSortModule,
      MatTableModule,
      MatTabsModule,
      MatToolbarModule,
      MatTooltipModule,
      MatStepperModule,
      MatFormFieldModule,
      FormsModule,
      ReactiveFormsModule,
      FormsModule

    ]
  })
  export class ApisModule { }
  