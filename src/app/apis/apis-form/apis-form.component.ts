

import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var jQuery: any;
@Component({
  selector: 'app-apis-form',
  templateUrl: './apis-form.component.html',
  styleUrls: ['./apis-form.component.css']
})
export class ApisFormComponent implements OnInit,AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // document.getElementById('preloader').classList.add('hide');
    jQuery('select').selectpicker();
    // jQuery('.multi-select').html( "<option value='elem_1'>elem 1</option>" );

  }  

}
