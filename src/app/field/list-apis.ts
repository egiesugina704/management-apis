export interface  ListApis {
// LIST_APIS Parameter
  api_id	: number;
  api_name	: string;
  sql_query	: string;
  tgl_create: string;
  active	: string;
  with_date	: string;
}