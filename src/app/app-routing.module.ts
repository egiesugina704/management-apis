import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApisComponent } from './apis/apis/apis.component';
import { ApisListComponent } from './apis/apis-list/apis-list.component';
import { ApisFormComponent } from './apis/apis-form/apis-form.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'apis',
    component: ApisComponent,
    canActivate: [AuthGuard],
    children: [
        {
            path: 'list',
            component: ApisListComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'form',
            component: ApisFormComponent,
            canActivate: [AuthGuard]
        }
    ]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '' },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
