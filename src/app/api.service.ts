import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ListApis } from './field/list-apis';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})

export class RestApiService  {

	apiURL = 'http://10.54.18.212:404/api';

	constructor(private http: HttpClient) { }
    httpOptions = {
    	headers: new HttpHeaders({
    		'Content-Type': 'application/json'
    	})
    }
    getAPI_LIST(): Observable<ListApis> {
    	return this.http.get<ListApis>(this.apiURL + '/LIST_APIS')
    	.pipe(
    		retry(1),
    		catchError(this.handleError)
    		)
	}
    handleError(error: Response | any) {
    	let errorMessage = '';
    	if(error.error instanceof ErrorEvent) {
    		errorMessage = error.error.message;
    	} else {
    		errorMessage = `Error Code: ${error}\nMessage: ${error.message}`;
    	}
    	window.alert(errorMessage);
    	return throwError(errorMessage);
    }
}