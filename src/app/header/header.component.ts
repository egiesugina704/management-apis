import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services';
import { Router } from '@angular/router';
import { User } from '../_models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser: User;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
    ) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
    }
    
    ngOnInit() {
    }
    
    logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
    }
    
  }
  